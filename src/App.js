import image from "./assets/images/devcampreact.png";

function App() {
  return (
    <div>
      <img src={image} alt="Devcamp React" width="1000"></img>
    </div>
  );
}

export default App;
